package com.example.telemarketer

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.app.role.RoleManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.telecom.PhoneAccountHandle
import android.telecom.TelecomManager
import android.telecom.TelecomManager.*
import android.telephony.SubscriptionManager
import android.telephony.TelephonyManager
import android.util.Log
import android.view.WindowManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import androidx.core.net.toUri
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_select_sim.*
import java.lang.reflect.Method


class MainActivity : AppCompatActivity() {
    private lateinit var phone: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        phone = editText_phoneNumber
    }

    override fun onStart() {
        super.onStart()
        dialer_call.setOnClickListener {
            offerReplacingDefaultDialer()
        }
        editText_phoneNumber.setOnEditorActionListener { _, _, _ ->
            offerReplacingDefaultDialer()
            true
        }
    }

    private fun offerReplacingDefaultDialer() {
        if (getSystemService(TelecomManager::class.java).defaultDialerPackage != packageName) {
            Log.d("default phone call", "false")
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                val rm = getSystemService(Context.ROLE_SERVICE) as RoleManager
                startActivityForResult(rm.createRequestRoleIntent(RoleManager.ROLE_DIALER), 120)
            }else {
                Intent(ACTION_CHANGE_DEFAULT_DIALER)
                    .putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packageName)
                    .let(::startActivity)
            }
        } else {
            Log.d("default phone call", "true, make call")
            makeCall()
        }
    }

    private fun makeCall() {
        if (PermissionChecker.checkSelfPermission(
                this,
                Manifest.permission.CALL_PHONE
            ) == PermissionChecker.PERMISSION_GRANTED && PermissionChecker.checkSelfPermission(
                this,
                Manifest.permission.READ_CALL_LOG
            ) == PermissionChecker.PERMISSION_GRANTED && PermissionChecker.checkSelfPermission(
                this,
                Manifest.permission.WRITE_CALL_LOG
            ) == PermissionChecker.PERMISSION_GRANTED && PermissionChecker.checkSelfPermission(
                this,
                Manifest.permission.WRITE_CONTACTS
            ) == PermissionChecker.PERMISSION_GRANTED && PermissionChecker.checkSelfPermission(
                this,
                Manifest.permission.READ_CONTACTS
            ) == PermissionChecker.PERMISSION_GRANTED && PermissionChecker.checkSelfPermission(
                this,
                Manifest.permission.MODIFY_AUDIO_SETTINGS
            ) == PermissionChecker.PERMISSION_GRANTED && PermissionChecker.checkSelfPermission(
                this,
                Manifest.permission.RECORD_AUDIO
            ) == PermissionChecker.PERMISSION_GRANTED && PermissionChecker.checkSelfPermission(
                this,
                Manifest.permission.MANAGE_OWN_CALLS
            ) == PermissionChecker.PERMISSION_GRANTED
        ) {
            val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            if(telephonyManager.phoneCount == 2){
                dialogSelectSim()
            }else{
                val uri = "tel:${phone.text}".toUri()
                startActivity(Intent(Intent.ACTION_CALL, uri))
            }
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.READ_CALL_LOG,
                    Manifest.permission.WRITE_CALL_LOG,
                    Manifest.permission.READ_CONTACTS,
                    Manifest.permission.WRITE_CONTACTS,
                    Manifest.permission.MODIFY_AUDIO_SETTINGS,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.MANAGE_OWN_CALLS
                ),
                REQUEST_PERMISSION
            )
        }
    }

    @SuppressLint("DiscouragedPrivateApi")
    fun dialogSelectSim(){
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_select_sim)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        val sim1 = dialog.dialog_sim1
        val sim2 = dialog.dialog_sim2
        val uri = "tel:${phone.text}".toUri()
        val intent = Intent(Intent.ACTION_CALL, uri)

        sim1.setOnClickListener {
            intent.putExtra("com.android.phone.extra.slot", 0)
            intent.putExtra("simSlot", 0)
            val simSlotIndex = 0 //first sim slot
            try {
                val getSubIdMethod: Method =
                    SubscriptionManager::class.java.getDeclaredMethod(
                        "getSubId",
                        Int::class.javaPrimitiveType
                    )
                getSubIdMethod.setAccessible(true)
                val subIdForSlot = (getSubIdMethod.invoke(
                    SubscriptionManager::class.java,
                    simSlotIndex
                ) as LongArray)[0]
                val componentName = ComponentName(
                    "com.android.phone",
                    "com.android.services.telephony.TelephonyConnectionService"
                )
                val phoneAccountHandle = PhoneAccountHandle(componentName, subIdForSlot.toString())
                intent.putExtra("android.telecom.extra.PHONE_ACCOUNT_HANDLE", phoneAccountHandle)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            startActivity(intent)
            dialog.dismiss()
        }
        sim2.setOnClickListener {
            intent.putExtra("com.android.phone.extra.slot", 1)
            intent.putExtra("simSlot", 1)
            val simSlotIndex = 1 //Second sim slot
            try {
                val getSubIdMethod: Method =
                    SubscriptionManager::class.java.getDeclaredMethod(
                        "getSubId",
                        Int::class.javaPrimitiveType
                    )
                getSubIdMethod.setAccessible(true)
                val subIdForSlot = (getSubIdMethod.invoke(
                    SubscriptionManager::class.java,
                    simSlotIndex
                ) as LongArray)[0]
                val componentName = ComponentName(
                    "com.android.phone",
                    "com.android.services.telephony.TelephonyConnectionService"
                )
                val phoneAccountHandle = PhoneAccountHandle(componentName, subIdForSlot.toString())
                intent.putExtra("android.telecom.extra.PHONE_ACCOUNT_HANDLE", phoneAccountHandle)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            startActivity(intent)
            dialog.dismiss()
        }
        dialog.show()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_PERMISSION && PermissionChecker.PERMISSION_GRANTED in grantResults) {
            makeCall()
        }
    }

    companion object {
        const val REQUEST_PERMISSION = 0
    }


}