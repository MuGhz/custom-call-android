package com.example.telemarketer

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.media.AudioManager
import android.os.Bundle
import android.provider.CallLog
import android.telecom.Call
import android.telecom.TelecomManager
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.amulyakhare.textdrawable.TextDrawable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_call.*
import java.util.concurrent.TimeUnit


class CallActivity : AppCompatActivity() {
    private lateinit var number: String
    private var loudSpeak = false
    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_call)
        number = intent.data!!.schemeSpecificPart
        var drawable = TextDrawable.builder().buildRound("JD", resources.getColor(R.color.grey))
        callActivity_icon.setImageDrawable(drawable)
    }

    override fun onStart() {
        super.onStart()
        callActivity_callEnd.setOnClickListener {
            OngoingCall.hangup()
        }
        callActivity_loudSpeak.setOnClickListener {
            runOnUiThread {
                loudSpeak = !loudSpeak
                val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
                audioManager.setMode(AudioManager.MODE_IN_CALL)
                audioManager.setSpeakerphoneOn(loudSpeak)
                Log.d("loudSpeak",loudSpeak.toString())
            }
        }

        OngoingCall.state
            .subscribe(::updateUi)
            .addTo(disposables)
//        OngoingCall.state
//            .filter{it == Call.STATE_SELECT_PHONE_ACCOUNT}
//            .firstElement()
//            .subscribe{
//                startActivity(Intent(TelecomManager.ACTION_CONFIGURE_PHONE_ACCOUNT))
//            }
//            .addTo(disposables)
        OngoingCall.state
            .filter { it == Call.STATE_DISCONNECTED }
            .delay(1, TimeUnit.SECONDS)
            .firstElement()
            .subscribe {
                //call ended
//                deleteCallLogByNumber(number)
                deleteLastCallLog(this,number)
            }
            .addTo(disposables)

    }

    @SuppressLint("SetTextI18n")
    private fun updateUi(state: Int) {
        callActivity_callInfo.text = "${state.asString().toLowerCase().capitalize()}\n"
        if(state == Call.STATE_ACTIVE)
        callActivity_callEnd.isVisible = state in listOf(
            Call.STATE_DIALING,
            Call.STATE_RINGING,
            Call.STATE_ACTIVE,
            Call.STATE_DISCONNECTED
        )
    }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    fun deleteCallLogByNumber(number: String) {
        try {
            val queryString = "NUMBER=$number"
            this.contentResolver.delete(CallLog.Calls.CONTENT_URI, queryString, null)
        }catch (e: Exception){
            Log.e("exception", e.toString())
        }

    }

    fun deleteLastCallLog(context: Context, phoneNumber: String) {
        try {
            //Thread.sleep(4000);
            val strNumberOne = arrayOf(phoneNumber)
            val cursor: Cursor? = context.contentResolver.query(
                CallLog.Calls.CONTENT_URI, null,
                CallLog.Calls.NUMBER + " = ? ", strNumberOne, CallLog.Calls.DATE + " DESC"
            )
            if (cursor!!.moveToFirst()) {
                val idOfRowToDelete: Int = cursor.getInt(cursor.getColumnIndex(CallLog.Calls._ID))
                val foo = context.contentResolver.delete(
                    CallLog.Calls.CONTENT_URI,
                    CallLog.Calls._ID + " = ? ", arrayOf(idOfRowToDelete.toString())
                )
            }
        } catch (ex: java.lang.Exception) {
            Log.v(
                "deleteNumber", "Exception, unable to remove # from call log: "
                        + ex.toString()
            )
        }
    }


    companion object {
        fun start(context: Context, call: Call) {
            Intent(context, CallActivity::class.java)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .setData(call.details.handle)
                .let(context::startActivity)
        }
    }
}